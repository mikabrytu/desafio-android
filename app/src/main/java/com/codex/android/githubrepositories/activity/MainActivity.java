package com.codex.android.githubrepositories.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.codex.android.githubrepositories.R;
import com.codex.android.githubrepositories.adapter.RepositoriesAdapter;
import com.codex.android.githubrepositories.model.Repository;
import com.codex.android.githubrepositories.network.Endpoints;
import com.codex.android.githubrepositories.network.Network;
import com.codex.android.githubrepositories.utils.EndlessRecyclerViewScrollListener;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private RepositoriesAdapter adapter;
    private EndlessRecyclerViewScrollListener scrollListener;
    private RecyclerView.LayoutManager manager;
    private ProgressBar progressBar;

    String lang = "Ruby";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initFields();
        configFields();
        prepareData(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchView mSearchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        mSearchView.setQueryHint("Ruby, Kotlin...");
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                loading(true);
                adapter.clearList();
                lang = query;
                prepareData(1);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initFields() {
        List<Repository> list = new ArrayList<>();

        recycler = (RecyclerView) findViewById(R.id.list);
        progressBar = (ProgressBar) findViewById(R.id.progress_repo_list);
        adapter = new RepositoriesAdapter(this, list);
        manager = new LinearLayoutManager(this);
        scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                prepareData(page + 1);
            }
        };
    }

    private void configFields() {
        recycler.setLayoutManager(manager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.addOnScrollListener(scrollListener);
        recycler.setAdapter(adapter);
    }

    private void prepareData(int page) {
        loading(true);

        // Call API
        String url = Endpoints.GET_REPOSITORIES;
        url = url.replace("%LANG%", lang);
        url = url.replace("%PAGE%", String.valueOf(page));
        Log.d("GET_REPOS", url);

        Network network = new Network();
        network.request(this, Request.Method.GET, url, new Network.Listener() {
            @Override
            public void onResponse(Object response) {

                JsonObject object = new JsonParser().parse(response.toString()).getAsJsonObject();
                JsonArray array = object.get("items").getAsJsonArray();

                List<Repository> list = new ArrayList<>();
                for (int i = 0; i < array.size(); i++) {
                    JsonObject item = array.get(i).getAsJsonObject();

                    Repository repository = new Repository();
                    repository.setRepoName(item.get("name").getAsString());
                    repository.setDescription(item.get("description").getAsString());
                    repository.setStars(item.get("stargazers_count").getAsInt());
                    repository.setForks(item.get("forks").getAsInt());
                    repository.setUserName(item.get("owner").getAsJsonObject().get("login").getAsString());
                    repository.setUserPic(item.get("owner").getAsJsonObject().get("avatar_url").getAsString());

                    list.add(repository);
                }

                adapter.updateList(list);
                loading(false);
            }

            @Override
            public void onError(Object error) {
                Log.e("ERROR", error.toString());
            }

            @Override
            public Map<String, String> getParams() {
                return null;
            }
        });
    }

    private void loading(boolean loading) {
        if (loading) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }
}
