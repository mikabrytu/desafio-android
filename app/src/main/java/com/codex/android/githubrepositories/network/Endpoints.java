package com.codex.android.githubrepositories.network;

/**
 * Created by mikabrytu on 21/06/17.
 */

public class Endpoints {
    public static String GET_REPOSITORIES = "https://api.github.com/search/repositories?q=language:%LANG%&sort=stars&page=%PAGE%";
    public static String GET_PULL_REQUESTS = "https://api.github.com/repos/%USER%/%REPO%/pulls?page=%PAGE%";
}
