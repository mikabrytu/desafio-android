package com.codex.android.githubrepositories.activity;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.codex.android.githubrepositories.R;
import com.codex.android.githubrepositories.adapter.PullRequestAdapter;
import com.codex.android.githubrepositories.model.PullRequest;
import com.codex.android.githubrepositories.network.Endpoints;
import com.codex.android.githubrepositories.network.Network;
import com.codex.android.githubrepositories.utils.EndlessRecyclerViewScrollListener;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PullRequestActivity extends AppCompatActivity {

    private RecyclerView recycler;
    private PullRequestAdapter adapter;
    private EndlessRecyclerViewScrollListener scrollListener;
    private RecyclerView.LayoutManager manager;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra("REPO_NAME"));

        initFields();
        configFields();
        prepareData(1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void initFields() {
        List<PullRequest> list = new ArrayList<>();

        recycler = (RecyclerView) findViewById(R.id.list);
        progressBar = (ProgressBar) findViewById(R.id.progress);
        adapter = new PullRequestAdapter(this, list);
        manager = new LinearLayoutManager(this);
        scrollListener = new EndlessRecyclerViewScrollListener((LinearLayoutManager) manager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                prepareData(page + 1);
            }
        };
    }

    private void configFields() {
        recycler.setLayoutManager(manager);
        recycler.setItemAnimator(new DefaultItemAnimator());
        recycler.addOnScrollListener(scrollListener);
        recycler.setAdapter(adapter);
    }

    private void prepareData(int page) {
        loading(true);

        String url = Endpoints.GET_PULL_REQUESTS;
        url = url.replace("%USER%", getIntent().getStringExtra("REPO_USER"));
        url = url.replace("%REPO%", getIntent().getStringExtra("REPO_NAME"));
        url = url.replace("%PAGE%", String.valueOf(page));
        Log.d("REQUEST_URL", url);

        Network network = new Network();
        network.request(this, Request.Method.GET, url, new Network.Listener() {
            @Override
            public void onResponse(Object response) {
                JsonArray array = new JsonParser().parse(response.toString()).getAsJsonArray();
                Log.d("RESPONSE", response.toString());

                List<PullRequest> list = new ArrayList<>();

                for (int i = 0; i < array.size(); i++) {
                    JsonObject object = array.get(i).getAsJsonObject();

                    PullRequest pull = new PullRequest();
                    pull.setTitle(object.get("title").getAsString());
                    pull.setBody(object.get("body").getAsString());
                    pull.setDate(object.get("created_at").getAsString());
                    pull.setUserName(object.get("user").getAsJsonObject().get("login").getAsString());
                    pull.setUserPic(object.get("user").getAsJsonObject().get("avatar_url").getAsString());

                    list.add(pull);
                }

                adapter.updateList(list);
                loading(false);
            }

            @Override
            public void onError(Object error) {
                Log.e("ERROR", error.toString());
            }

            @Override
            public Map<String, String> getParams() {
                return null;
            }
        });
    }

    private void loading(boolean isLoading) {
        if (isLoading)
            progressBar.setVisibility(View.VISIBLE);
        else
            progressBar.setVisibility(View.GONE);
    }
}
