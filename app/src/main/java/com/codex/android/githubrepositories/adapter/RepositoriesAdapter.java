package com.codex.android.githubrepositories.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.codex.android.githubrepositories.R;
import com.codex.android.githubrepositories.activity.PullRequestActivity;
import com.codex.android.githubrepositories.model.Repository;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by mikabrytu on 21/06/17.
 */

public class RepositoriesAdapter extends RecyclerView.Adapter<RepositoriesAdapter.ViewHolder> {

    private Context context;
    private List<Repository> list;

    public RepositoriesAdapter(Context context, List<Repository> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.inflate_repository_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Repository item = list.get(position);

        holder.repoName.setText(item.getRepoName());
        holder.description.setText(item.getDescription());
        holder.userName.setText(item.getUserName());
        holder.stars.setText(String.valueOf(item.getStars()));
        holder.forks.setText(String.valueOf(item.getForks()));

        Picasso.with(context).load(item.getUserPic()).placeholder(R.drawable.ic_account_circle_amber_700_48dp).into(holder.userPic);

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PullRequestActivity.class);
                intent.putExtra("REPO_NAME", item.getRepoName());
                intent.putExtra("REPO_USER", item.getUserName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<Repository> list) {
        this.list.addAll(list);
        this.notifyDataSetChanged();
    }

    public void clearList() {
        this.list.clear();
        this.notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout view;
        TextView repoName, description, stars, forks, userName;
        ImageView userPic;

        ViewHolder(View view) {
            super(view);

            this.view = (LinearLayout) view.findViewById(R.id.view_repo);
            repoName = (TextView) view.findViewById(R.id.text_rep_name);
            description = (TextView) view.findViewById(R.id.text_rep_desc);
            stars = (TextView) view.findViewById(R.id.text_rep_stars);
            forks = (TextView) view.findViewById(R.id.text_rep_fork);
            userName = (TextView) view.findViewById(R.id.text_rep_username);
            userPic = (ImageView) view.findViewById(R.id.image_rep_userpic);
        }
    }
}
