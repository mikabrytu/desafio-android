package com.codex.android.githubrepositories.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.codex.android.githubrepositories.R;
import com.codex.android.githubrepositories.model.PullRequest;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by mikabrytu on 22/06/17.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder> {

    private Context context;
    private List<PullRequest> list;

    public PullRequestAdapter(Context context, List<PullRequest> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.inflate_pull_request_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PullRequest item = list.get(position);

        holder.title.setText(item.getTitle());
        holder.body.setText(item.getBody());
        holder.userName.setText(item.getUserName());

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = simpleDateFormat.parse(item.getDate().substring(0, 10));
            simpleDateFormat.applyPattern("dd/MM/yyyy");
            String dateFormated = simpleDateFormat.format(date);

            holder.date.setText(dateFormated);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Picasso.with(context).load(item.getUserPic()).placeholder(R.drawable.ic_account_circle_amber_700_48dp).into(holder.userPic);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateList(List<PullRequest> list) {
        this.list.addAll(list);
        this.notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView title, body, userName, date;
        ImageView userPic;

        ViewHolder(View view) {
            super(view);

            title = (TextView) view.findViewById(R.id.text_pull_title);
            body = (TextView) view.findViewById(R.id.text_pull_body);
            userName = (TextView) view.findViewById(R.id.text_pull_username);
            date = (TextView) view.findViewById(R.id.text_pull_date);
            userPic = (ImageView) view.findViewById(R.id.image_pull_userpic);
        }
    }

}
